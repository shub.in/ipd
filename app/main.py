import logging

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from .config import Config
from .database import DBType, DB, MongoDB
from .store import Store
from .store.mongo import MongoStore
from .services import Service
from .routers import router


class App(FastAPI):
    config: Config
    db: DB
    store: Store
    services: Service


def startup(app: App, config: Config):
    async def f():
        logging.info('Startup')
        app.config = config
        if config.db.scheme == DBType.MONGODB:
            app.db = MongoDB(dsn=config.db, dbname=config.mongodb_name)
            await app.db.connect()
            app.store = MongoStore(db=app.db.connection)
        else:
            raise Exception('Unsupported database scheme')
        app.services = Service(store=app.store, config=app.config)
    return f


def shutdown(app: App, config: Config):
    async def f():
        await app.db.disconnect()
    return f


def make_app(config: Config) -> FastAPI:
    api_prefix = config.api_prefix
    app = App(title=config.title,
              version=config.version,
              openapi_url=f'{api_prefix}/openapi.json' if config.docs else None,
              docs_url=f'{api_prefix}/docs' if config.docs else None,
              redoc_url=f'{api_prefix}/redoc' if config.docs else None)
    app.add_middleware(CORSMiddleware,
                       allow_credentials=True,
                       allow_origins=['*'],
                       allow_methods=['*'],
                       allow_headers=['*'])
    app.add_event_handler('startup', startup(app=app, config=config))
    app.add_event_handler('shutdown', shutdown(app=app, config=config))
    app.include_router(router, prefix=config.api_prefix)

    return app

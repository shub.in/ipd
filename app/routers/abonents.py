from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends
from starlette import status

from app.dependencies import pagination_dep, services_dep
from app.services import Service
from app.schemas.errors import Error
from app.schemas.utils import Pagination
from app.schemas.abonents import Abonent, AbonentRequest, AbonentUpdateRequest

router = APIRouter()


@router.get('',
            status_code=status.HTTP_200_OK,
            response_model=List[Abonent],
            responses={500: {'model': Error}})
async def list(services: Service = Depends(services_dep), pagination: Pagination = Depends(pagination_dep)):
    return await services.abonents.list(pagination=pagination)


@router.post('',
             status_code=status.HTTP_201_CREATED,
             response_model=Abonent,
             responses={500: {'model': Error}})
async def create(request: AbonentRequest, services: Service = Depends(services_dep)):
    return await services.abonents.create(item=request)


@router.get('/{id}',
            status_code=status.HTTP_200_OK,
            response_model=Abonent,
            responses={500: {'model': Error}})
async def get(id: UUID, services: Service = Depends(services_dep)):
    return await services.abonents.get(id=id)


@router.patch('/{id}',
              status_code=status.HTTP_200_OK,
              response_model=Abonent,
              responses={500: {'model': Error}})
async def update(id: UUID, request: AbonentUpdateRequest, services: Service = Depends(services_dep)):
    return await services.abonents.update(id=id, item=request)


@router.delete('/{id}',
               status_code=status.HTTP_204_NO_CONTENT,
               response_model=None,
               responses={500: {'model': Error}})
async def delete(id: UUID, services: Service = Depends(services_dep)):
    return await services.abonents.delete(id=id)

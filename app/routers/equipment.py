from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends
from starlette import status

from app.dependencies.services import services_dep
from app.services import Service
from app.schemas.errors import Error
from app.schemas.equipment import Equipment, EquipmentRequest, EquipmentUpdateRequest

router = APIRouter()


@router.get('',
            status_code=status.HTTP_200_OK,
            response_model=List[Equipment],
            responses={500: {'model': Error}})
async def list(services: Service = Depends(services_dep)):
    return await services.equipment.list()


@router.post('',
             status_code=status.HTTP_201_CREATED,
             response_model=Equipment,
             responses={500: {'model': Error}})
async def create(request: EquipmentRequest, services: Service = Depends(services_dep)):
    return await services.equipment.create(item=request)


@router.get('/{id}',
            status_code=status.HTTP_200_OK,
            response_model=Equipment,
            responses={500: {'model': Error}})
async def get(id: UUID, services: Service = Depends(services_dep)):
    return await services.equipment.get(id=id)


@router.patch('/{id}',
              status_code=status.HTTP_200_OK,
              response_model=Equipment,
              responses={500: {'model': Error}})
async def update(id: UUID, request: EquipmentUpdateRequest, services: Service = Depends(services_dep)):
    return await services.equipment.update(id=id, item=request)


@router.delete('/{id}',
               status_code=status.HTTP_204_NO_CONTENT,
               response_model=None,
               responses={500: {'model': Error}})
async def delete(id: UUID, services: Service = Depends(services_dep)):
    return await services.equipment.delete(id=id)

from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends
from starlette import status

from ..dependencies.services import services_dep
from ..services import Service
from ..schemas.errors import Error
from ..schemas.agreements import Agreement, AgreementRequest, AgreementUpdateRquest

from .utils import response

router = APIRouter()


@router.get('',
            status_code=status.HTTP_200_OK,
            response_model=List[Agreement],
            responses={500: {'model': Error}})
async def list(services: Service = Depends(services_dep)):
    return await services.agreements.list()


@router.post('',
             status_code=status.HTTP_201_CREATED,
             response_model=Agreement,
             responses={500: {'model': Error}})
async def create(request: AgreementRequest, services: Service = Depends(services_dep)):
    data, error = await services.agreements.create(item=request)
    return response(data, error)


@router.get('/{id}',
            status_code=status.HTTP_200_OK,
            response_model=Agreement,
            responses={500: {'model': Error}})
async def get(id: UUID, services: Service = Depends(services_dep)):
    return await services.agreements.get(id=id)


@router.patch('/{id}',
              status_code=status.HTTP_200_OK,
              response_model=Agreement,
              responses={500: {'model': Error}})
async def update(id: UUID, request: AgreementUpdateRquest, services: Service = Depends(services_dep)):
    return await services.agreements.update(id=id, item=request)


@router.delete('/{id}',
               status_code=status.HTTP_204_NO_CONTENT,
               response_model=None,
               responses={500: {'model': Error}})
async def delete(id: UUID, services: Service = Depends(services_dep)):
    return await services.agreements.delete(id=id)

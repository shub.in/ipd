from fastapi import HTTPException

from app.schemas.errors import Error


def response(data, error: Error):
    if error is not None:
        raise HTTPException(detail=error.detail, status_code=error.status_code)
    return data

from fastapi import APIRouter

from . import auth, abonents, agreements, equipment, tariffs, users

router = APIRouter()
router.include_router(auth.router, prefix='/auth', tags=['auth'])
router.include_router(abonents.router, prefix='/abonents', tags=['abonents'])
router.include_router(agreements.router, prefix='/agreements', tags=['agreements'])
router.include_router(equipment.router, prefix='/equipment', tags=['equipment'])
router.include_router(tariffs.router, prefix='/tariffs', tags=['tariffs'])
router.include_router(users.router, prefix='/users', tags=['users'])

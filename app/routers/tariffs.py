from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends
from starlette import status

from app.dependencies.services import services_dep
from app.services import Service
from app.schemas.errors import Error
from app.schemas.tariffs import Tariff, TariffRequest, TariffUpdateRequest

router = APIRouter()


@router.get('',
            status_code=status.HTTP_200_OK,
            response_model=List[Tariff],
            responses={500: {'model': Error}})
async def list(services: Service = Depends(services_dep)):
    return await services.tariffs.list()


@router.post('',
             status_code=status.HTTP_201_CREATED,
             response_model=Tariff,
             responses={500: {'model': Error}})
async def create(request: TariffRequest, services: Service = Depends(services_dep)):
    return await services.tariffs.create(item=request)


@router.get('/{id}',
            status_code=status.HTTP_200_OK,
            response_model=Tariff,
            responses={500: {'model': Error}})
async def get(id: UUID, services: Service = Depends(services_dep)):
    return await services.tariffs.get(id=id)


@router.patch('/{id}',
              status_code=status.HTTP_200_OK,
              response_model=Tariff,
              responses={500: {'model': Error}})
async def update(id: UUID, request: TariffUpdateRequest, services: Service = Depends(services_dep)):
    return await services.tariffs.update(id=id, item=request)


@router.delete('/{id}',
               status_code=status.HTTP_204_NO_CONTENT,
               response_model=None,
               responses={500: {'model': Error}})
async def delete(id: UUID, services: Service = Depends(services_dep)):
    return await services.tariffs.delete(id=id)

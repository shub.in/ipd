from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends, Query, HTTPException
from starlette import status

from app.schemas.auth import Credentials, Tokens
from app.dependencies.services import services_dep
from app.services import Service
from app.schemas.errors import Error
from .utils import response

router = APIRouter()


@router.get('/bcrypt',
            status_code=status.HTTP_200_OK,
            response_model=bytes,
            responses={500: {'model': Error}})
async def bcrypt(password: str = Query(...), services: Service = Depends(services_dep)):
    return services.auth.bcrypt(password=password)


@router.post('',
             status_code=status.HTTP_200_OK,
             response_model=Tokens,
             responses={500: {'model': Error}})
async def login(credentials: Credentials, services: Service = Depends(services_dep)):
    data, error = await services.auth.login(credentials=credentials)
    return response(data, error)

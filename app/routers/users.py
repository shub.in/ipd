from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends
from starlette import status

from app.dependencies.services import services_dep
from app.services import Service
from app.schemas.errors import Error
from app.schemas.users import User, UserRequest, UserUpdateRequest, PasswordUpdateRequest

router = APIRouter()


@router.get('',
            status_code=status.HTTP_200_OK,
            response_model=List[User],
            responses={500: {'model': Error}})
async def list(services: Service = Depends(services_dep)):
    return await services.users.list()


@router.post('',
             status_code=status.HTTP_201_CREATED,
             response_model=User,
             responses={500: {'model': Error}})
async def create(request: UserRequest, services: Service = Depends(services_dep)):
    return await services.users.create(item=request)


@router.get('/{id}',
            status_code=status.HTTP_200_OK,
            response_model=User,
            responses={500: {'model': Error}})
async def get(id: UUID, services: Service = Depends(services_dep)):
    return await services.users.get(id=id)


@router.patch('/{id}',
              status_code=status.HTTP_200_OK,
              response_model=User,
              responses={500: {'model': Error}})
async def update(id: UUID, request: UserUpdateRequest, services: Service = Depends(services_dep)):
    return await services.users.update(id=id, item=request)


@router.post('/{id}/change_password',
             status_code=status.HTTP_200_OK,
             response_model=None,
             responses={500: {'model': Error}})
async def change_password(id: UUID, request: PasswordUpdateRequest, services: Service = Depends(services_dep)):
    return await services.users.change_password(id=id, item=request)


@router.delete('/{id}',
               status_code=status.HTTP_204_NO_CONTENT,
               response_model=None,
               responses={500: {'model': Error}})
async def delete(id: UUID, services: Service = Depends(services_dep)):
    return await services.users.delete(id=id)

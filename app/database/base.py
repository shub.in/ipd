from abc import ABC, abstractmethod
from enum import Enum


class DBType(str, Enum):
    MONGODB = 'mongodb'


class DB(ABC):
    @abstractmethod
    async def connect(self):
        pass

    @abstractmethod
    async def disconnect(self):
        pass

    @property
    @abstractmethod
    def connection(self):
        pass

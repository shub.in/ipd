from motor.motor_asyncio import AsyncIOMotorClient
from motor.core import AgnosticDatabase

from app.config import MongoDsn

from .base import DB


class MongoDB(DB):
    dsn: MongoDsn
    dbname: str
    _client: AsyncIOMotorClient
    _db: AgnosticDatabase

    def __str__(self):
        return f'<MongoDB dsn: {self.dsn}, db: {self.dbname}>'

    def __init__(self, dsn: MongoDsn, dbname: str):
        self.dsn = dsn
        self.dbname = dbname

    async def connect(self):
        self._client = AsyncIOMotorClient(self.dsn)
        self._db = self._client[self.dbname]

    async def disconnect(self):
        pass

    @property
    def connection(self):
        return self._db

from typing import Optional

from fastapi import Request

from app.schemas.utils import Pagination


def get_offset(page: int = 1, limit: int = 20):
    offset = (page - 1) * limit
    return offset


async def pagination_dep(request: Request, search: Optional[str] = None, page: Optional[int] = 1) -> Pagination:
    limit = request.app.config.page_size
    offset = get_offset(page=page, limit=limit)
    return Pagination(page=page,
                      limit=limit,
                      offset=offset,
                      search=search)

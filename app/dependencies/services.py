from fastapi import Request

from app.services import Service


async def services_dep(request: Request) -> Service:
    return request.app.services

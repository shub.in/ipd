from fastapi import Request

from app.store import Store


async def store_dep(request: Request) -> Store:
    return request.app.store

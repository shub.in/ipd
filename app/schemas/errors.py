from pydantic import BaseModel


class Error(BaseModel):
    detail: str
    status_code: int

from pydantic import BaseModel


class Credentials(BaseModel):
    username: str
    password: str


class Tokens(BaseModel):
    access_token: str
    refresh_token: str

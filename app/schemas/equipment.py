from typing import Optional
from pydantic import BaseModel, Field

from .base import Base


class EquipmentRequest(BaseModel):
    title: str
    price: float
    initial_fee: float
    is_available: bool = Field(default=True)
    data: Optional[dict]


class EquipmentUpdateRequest(BaseModel):
    title: Optional[str]
    price: Optional[float]
    initial_fee: Optional[float]
    is_available: Optional[bool]
    data: Optional[dict]


class Equipment(EquipmentRequest, Base):
    pass

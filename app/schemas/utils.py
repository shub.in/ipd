from typing import Optional

from pydantic import BaseModel, Field


class Pagination(BaseModel):
    page: int = Field(default=1)
    limit: int
    offset: int
    sort: Optional[str]
    search: Optional[str]

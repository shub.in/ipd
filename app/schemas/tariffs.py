from uuid import UUID
from typing import List, Optional
from pydantic import BaseModel, Field

from .base import Base
from .equipment import Equipment


class TariffRequest(BaseModel):
    title: str
    period: int = Field(..., gt=0)
    is_available: bool = Field(default=True)
    applicable_to: Optional[List[UUID]]
    data: Optional[dict]


class TariffUpdateRequest(BaseModel):
    title: Optional[str]
    period: Optional[int]
    is_available: Optional[bool]
    applicable_to: Optional[List[UUID]]
    data: Optional[dict]


class Tariff(TariffRequest, Base):
    applicable_to: Optional[List[UUID]]
    equipment: Optional[List[Equipment]]

from uuid import uuid4, UUID
from typing import Optional
from datetime import datetime

from pydantic import BaseModel, Field


class Base(BaseModel):
    id: UUID = Field(default_factory=uuid4, alias='_id')
    created_at: datetime = Field(default_factory=datetime.utcnow)
    updated_at: datetime = Field(default_factory=datetime.utcnow)
    created_by: Optional[UUID] = Field(default_factory=uuid4)
    updated_by: Optional[UUID] = Field(default_factory=uuid4)

    class Config:
        allow_population_by_field_name = True
        use_enum_values = True


def update_model(model: BaseModel):
    print(model.schema())


if __name__ == '__main__':
    update_model(Base)

from uuid import UUID
from enum import Enum
from typing import List, Optional
from pydantic import BaseModel, Field

from .base import Base
from .equipment import Equipment


class Role(str, Enum):
    ADMIN = 'admin'
    MANAGER = 'manager'


class UserRequest(BaseModel):
    username: str
    password: bytes
    fullname: Optional[str]
    role: Role = Field(default=Role.MANAGER)
    is_active: bool = Field(default=True)


class UserUpdateRequest(BaseModel):
    username: Optional[str]
    fullname: Optional[str]
    role: Optional[Role]
    is_active: Optional[bool]


class PasswordUpdateRequest(BaseModel):
    password: str
    new_password: str
    new_password_repeat: str


class User(UserRequest, Base):
    pass

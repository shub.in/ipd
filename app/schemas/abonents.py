from typing import Optional
from enum import Enum

from pydantic import BaseModel, Field

from .base import Base


class Gender(str, Enum):
    MALE = 'male'
    FEMALE = 'female'


class AbonentRequest(BaseModel):
    fname: str
    lname: str
    mname: Optional[str]
    gender: Gender = Field(default=Gender.MALE)
    phone: str
    passport_series: str = Field(..., min_length=4, max_length=4)
    passport_number: str = Field(..., min_length=6, max_length=6)
    passport_issuer: str
    data: Optional[dict]


class AbonentUpdateRequest(BaseModel):
    fname: Optional[str]
    lname: Optional[str]
    mname: Optional[str]
    gender: Optional[Gender]
    phone: Optional[str]
    passport_series: Optional[str]
    passport_number: Optional[str]
    passport_issuer: Optional[str]
    data: Optional[dict]


class Abonent(AbonentRequest, Base):
    pass

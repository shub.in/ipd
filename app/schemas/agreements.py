from uuid import UUID
from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel, Field

from .base import Base


class Payment(BaseModel):
    datetime: datetime
    is_paid: bool = Field(default=False)


class PaymentRequest(BaseModel):
    is_paid: bool


class AgreementRequest(BaseModel):
    abonent_id: UUID
    equipmment: List[UUID]
    tarif_id: UUID


class AgreementUpdateRquest(BaseModel):
    equipment: Optional[List[UUID]]
    tarif_id: Optional[UUID]


class Agreement(AgreementRequest, Base):
    number: str
    payments: List[Payment]

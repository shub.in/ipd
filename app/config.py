import os
from typing import Union, Optional

import yaml
from pydantic import BaseSettings, PostgresDsn, Field, AnyUrl


class MongoDsn(AnyUrl):
    allowed_schemes = {'mongodb'}
    user_required = True


class Config(BaseSettings):
    title: str = Field(default='App')
    docs: bool = Field(default=True)
    db: Union[PostgresDsn, MongoDsn]
    mongodb_name: Optional[str]
    host: str = Field(default='127.0.0.1')
    port: int = Field(default=8000)
    api_prefix: str = Field(default='')
    version: str = Field(default='0.1.0')
    jwt_key: str = Field(default='Qwerty123')
    page_size: int = Field(default=20)


def read_config() -> Config:
    path: str = os.environ.get('CONFIG_PATH', 'config.yaml')
    if not os.path.exists(path=path):
        raise Exception(f'Configurations file does not exists: {path}')
    with open(path) as f:
        raw = yaml.safe_load(f)
        config = Config(**raw)
        return config


config: Config = read_config()

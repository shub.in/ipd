from typing import List
from uuid import UUID

from app.schemas.tariffs import Tariff, TariffRequest, TariffUpdateRequest
from .base import BaseService


class TariffService(BaseService):
    async def list(self) -> List[Tariff]:
        return await self.store.tariffs.list()

    async def create(self, item: TariffRequest) -> Tariff:
        abonent = Tariff(**item.dict(by_alias=True))
        return await self.store.tariffs.create(item=abonent)

    async def get(self, id: UUID) -> Tariff:
        return await self.store.tariffs.get(id=id)

    async def update(self, id: UUID, item: TariffUpdateRequest) -> Tariff:
        return await self.store.tariffs.update(id=id, item=item)

    async def delete(self, id: UUID) -> None:
        await self.store.tariffs.delete(id=id)
        return None

from typing import List
from uuid import UUID
from datetime import datetime, timedelta

from starlette import status
from faker import Faker

from ..schemas.agreements import Agreement, AgreementRequest, AgreementUpdateRquest, Payment
from ..schemas.errors import Error

from .base import BaseService


class AgreementService(BaseService):
    async def list(self) -> List[Agreement]:
        return await self.store.agreements.list()

    async def create(self, item: AgreementRequest) -> (Agreement, Error):
        abonent = await self.store.abonents.get(id=item.abonent_id)
        print(abonent)
        if abonent is None:
            return None, Error(detail='Abonent not found', status_code=status.HTTP_400_BAD_REQUEST)
        tariff = await self.store.tariffs.get(id=item.tarif_id)
        if tariff is None:
            return None, Error(detail='Tariff not found', status_code=status.HTTP_400_BAD_REQUEST)
        if not set(item.equipmment).issubset(tariff.applicable_to):
            return None, Error(detail='Not suitable tariff', status_code=status.HTTP_400_BAD_REQUEST)
        f = Faker()
        payments: List[Payment] = []
        ts = datetime.utcnow()
        for index in range(tariff.period):
            payments.append(Payment(datetime=ts + timedelta(weeks=4 * (index + 1))))
        agreement = Agreement(**item.dict(by_alias=True),
                              number=f.random_int(min=10000, max=99999),
                              payments=payments)
        return await self.store.agreements.create(item=agreement), None

    async def get(self, id: UUID) -> Agreement:
        return await self.store.agreements.get(id=id)

    async def update(self, id: UUID, item: AgreementUpdateRquest) -> Agreement:
        return await self.store.agreements.update(id=id, item=item)

    async def delete(self, id: UUID) -> None:
        await self.store.agreements.delete(id=id)
        return None

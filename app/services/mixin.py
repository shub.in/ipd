from bcrypt import gensalt, checkpw, hashpw


class PasswordMixin:
    @staticmethod
    def _check_password(password: str, hashed_password: bytes) -> bool:
        return checkpw(password=password.encode(), hashed_password=hashed_password)

    @staticmethod
    def _hash_password(password: bytes) -> bytes:
        return hashpw(password=password, salt=gensalt(rounds=10))

from starlette import status
import jwt

from ..schemas.auth import Credentials, Tokens
from ..schemas.users import User
from ..schemas.errors import Error

from .base import BaseService
from .mixin import PasswordMixin


class AuthService(BaseService, PasswordMixin):
    def bcrypt(self, password: str) -> bytes:
        return self._hash_password(password=password.encode())

    async def login(self, credentials: Credentials) -> (Tokens, Error):
        user = await self.store.users.get_by_username(username=credentials.username)
        print(user)
        if user is None or not self._check_password(password=credentials.password,
                                                    hashed_password=user.password):
            return None, Error(detail='Bad credentials', status_code=status.HTTP_401_UNAUTHORIZED)
        token = jwt.encode(payload={'username': user.username,
                                    'role': user.role,
                                    'fullname': user.fullname},
                           key=self.config.jwt_key)
        return Tokens(access_token=token, refresh_token=''), None

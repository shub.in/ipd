from app.store import Store
from app.config import Config
from .auth import AuthService
from .abonents import AbonentService
from .agreements import AgreementService
from .equipment import EquipmentService
from .tariffs import TariffService
from .users import UserService


class Service:
    auth: AuthService
    abonents: AbonentService
    agreements: AgreementService
    equipment: EquipmentService
    tariffs: TariffService
    users: UserService

    def __init__(self, store: Store, config: Config):
        self.auth = AuthService(store=store, config=config)
        self.abonents = AbonentService(store=store, config=config)
        self.agreements = AgreementService(store=store, config=config)
        self.equipment = EquipmentService(store=store, config=config)
        self.tariffs = TariffService(store=store, config=config)
        self.users = UserService(store=store, config=config)

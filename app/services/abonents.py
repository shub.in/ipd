from typing import List
from uuid import UUID

from app.schemas.abonents import Abonent, AbonentRequest, AbonentUpdateRequest
from app.schemas.utils import Pagination
from .base import BaseService


class AbonentService(BaseService):
    async def list(self, pagination: Pagination) -> List[Abonent]:
        return await self.store.abonents.list(pagination=pagination)

    async def create(self, item: AbonentRequest) -> Abonent:
        abonent = Abonent(**item.dict(by_alias=True))
        return await self.store.abonents.create(item=abonent)

    async def get(self, id: UUID) -> Abonent:
        return await self.store.abonents.get(id=id)

    async def update(self, id: UUID, item: AbonentUpdateRequest) -> Abonent:
        return await self.store.abonents.update(id=id, item=item)

    async def delete(self, id: UUID) -> None:
        await self.store.abonents.delete(id=id)
        return None

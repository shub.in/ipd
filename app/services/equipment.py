from typing import List
from uuid import UUID

from app.schemas.equipment import Equipment, EquipmentRequest, EquipmentUpdateRequest
from .base import BaseService


class EquipmentService(BaseService):
    async def list(self) -> List[Equipment]:
        return await self.store.equipment.list()

    async def create(self, item: EquipmentRequest) -> Equipment:
        abonent = Equipment(**item.dict(by_alias=True))
        return await self.store.equipment.create(item=abonent)

    async def get(self, id: UUID) -> Equipment:
        return await self.store.equipment.get(id=id)

    async def update(self, id: UUID, item: EquipmentUpdateRequest) -> Equipment:
        return await self.store.equipment.update(id=id, item=item)

    async def delete(self, id: UUID) -> None:
        await self.store.equipment.delete(id=id)
        return None

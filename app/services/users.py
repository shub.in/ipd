from typing import List
from uuid import UUID

from starlette import status

from ..schemas.users import User, UserRequest, UserUpdateRequest, PasswordUpdateRequest
from ..schemas.errors import Error

from .mixin import PasswordMixin
from .base import BaseService


class UserService(BaseService, PasswordMixin):
    async def list(self) -> List[User]:
        return await self.store.users.list()

    async def create(self, item: UserRequest) -> User:
        item.password = self._hash_password(password=item.password)
        user = User(**item.dict(by_alias=True))
        return await self.store.users.create(item=user)

    async def get(self, id: UUID) -> User:
        return await self.store.users.get(id=id)

    async def update(self, id: UUID, item: UserUpdateRequest) -> User:
        return await self.store.users.update(id=id, item=item)

    async def change_password(self, id: UUID, item: PasswordUpdateRequest) -> User:
        user = await self.get(id=id)
        if not self._check_password(password=item.password, hashed_password=user.password):
            return Error(detail='Wrong old password', status_code=status.HTTP_400_BAD_REQUEST)
        if item.new_password != item.new_password_repeat:
            return Error(detail='Passwords don\'t match', status_code=status.HTTP_400_BAD_REQUEST)
        new_password = self._hash_password(password=item.new_password.encode())
        return await self.store.users.update_password(id=id, new_password=new_password)

    async def delete(self, id: UUID) -> None:
        await self.store.users.delete(id=id)
        return None

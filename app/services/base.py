from abc import ABC

from app.config import Config
from app.store import Store


class BaseService(ABC):
    store: Store
    config: Config

    def __init__(self, store: Store, config: Config):
        self.config = config
        self.store = store

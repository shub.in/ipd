from abc import ABC, abstractmethod
from typing import Any


class BaseStore(ABC):
    _db: Any

    @abstractmethod
    def __init__(self, db: Any):
        pass

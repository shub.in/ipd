from abc import abstractmethod, ABC
from uuid import UUID
from typing import List

from app.schemas.abonents import Abonent, AbonentUpdateRequest
from app.schemas.utils import Pagination

from .base import BaseStore


class AbonentStore(BaseStore, ABC):
    @abstractmethod
    async def list(self, pagination: Pagination) -> List[Abonent]:
        pass

    @abstractmethod
    async def create(self, item: Abonent) -> Abonent:
        pass

    @abstractmethod
    async def get(self, id: UUID) -> Abonent:
        pass

    @abstractmethod
    async def update(self, id: UUID, item: AbonentUpdateRequest) -> Abonent:
        pass

    @abstractmethod
    async def delete(self, id: UUID) -> None:
        pass

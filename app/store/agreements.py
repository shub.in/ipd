from abc import abstractmethod, ABC
from uuid import UUID
from typing import List

from ..schemas.agreements import Agreement, AgreementUpdateRquest

from .base import BaseStore


class AgreementStore(BaseStore, ABC):
    @abstractmethod
    async def list(self) -> List[Agreement]:
        pass

    @abstractmethod
    async def create(self, item: Agreement) -> Agreement:
        pass

    @abstractmethod
    async def get(self, id: UUID) -> Agreement:
        pass

    @abstractmethod
    async def update(self, id: UUID, item: AgreementUpdateRquest) -> Agreement:
        pass

    @abstractmethod
    async def delete(self, id: UUID) -> None:
        pass

from abc import ABC

from .abonents import AbonentStore
from .agreements import AgreementStore
from .equipment import EquipmentStore
from .tariffs import TariffStore
from .users import UsersStore


class Store(ABC):
    abonents: AbonentStore
    agreements: AgreementStore
    equipment: EquipmentStore
    tariffs: TariffStore
    users: UsersStore

from abc import abstractmethod, ABC
from uuid import UUID
from typing import List

from app.schemas.tariffs import Tariff, TariffUpdateRequest

from .base import BaseStore


class TariffStore(BaseStore, ABC):
    @abstractmethod
    async def list(self) -> List[Tariff]:
        pass

    @abstractmethod
    async def create(self, item: Tariff) -> Tariff:
        pass

    @abstractmethod
    async def get(self, id: UUID) -> Tariff:
        pass

    @abstractmethod
    async def update(self, id: UUID, item: TariffUpdateRequest) -> Tariff:
        pass

    @abstractmethod
    async def delete(self, id: UUID) -> None:
        pass

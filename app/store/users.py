from abc import abstractmethod, ABC
from uuid import UUID
from typing import List

from app.schemas.users import User, UserRequest, UserUpdateRequest

from .base import BaseStore


class UsersStore(BaseStore, ABC):
    @abstractmethod
    async def list(self) -> List[User]:
        pass

    @abstractmethod
    async def create(self, item: UserRequest) -> User:
        pass

    @abstractmethod
    async def get(self, id: UUID) -> User:
        pass

    @abstractmethod
    async def get_by_username(self, username: str) -> User:
        pass

    @abstractmethod
    async def update(self, id: UUID, item: UserUpdateRequest) -> User:
        pass

    @abstractmethod
    async def update_password(self, id: UUID, new_password: bytes) -> User:
        pass

    @abstractmethod
    async def delete(self, id: UUID) -> None:
        pass

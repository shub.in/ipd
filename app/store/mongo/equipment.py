from uuid import UUID
from typing import List

from app.schemas.equipment import Equipment, EquipmentRequest, EquipmentUpdateRequest
from app.store.equipment import EquipmentStore

from .base import MongoBaseStore


class EquipmentMongoStore(EquipmentStore, MongoBaseStore):
    _collection = 'equipment'

    async def list(self) -> List[Equipment]:
        return [Equipment(**item) async for item in self.collection.find()]

    async def create(self, item: Equipment) -> Equipment:
        result = await self.collection.insert_one(item.dict(by_alias=True))
        return await self.get(id=item.id)

    async def get(self, id: UUID) -> Equipment:
        document = await self.collection.find_one({'_id': id})
        return Equipment(**document) if document is not None else None

    async def update(self, id: UUID, item: EquipmentUpdateRequest) -> Equipment:
        result = await self.collection.update_one(
            {'_id': id},
            {
                '$set': item.dict(exclude_unset=True),
                '$currentDate': {'updated_at': True}
            }
        )
        return await self.get(id=id)

    async def delete(self, id: UUID) -> None:
        await self.collection.delete_one({'_id': id})
        return None

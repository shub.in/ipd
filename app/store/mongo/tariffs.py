from uuid import UUID
from typing import List

from pymongo.database import DBRef

from app.schemas.tariffs import Tariff, TariffRequest, TariffUpdateRequest
from app.schemas.equipment import Equipment
from app.store.tariffs import TariffStore

from .base import MongoBaseStore


class TariffMongoStore(TariffStore, MongoBaseStore):
    _collection = 'tariffs'

    async def list(self) -> List[Tariff]:
        pipeline = [
            {
                '$lookup':
                    {
                        'from': 'equipment',
                        'localField': 'applicable_to',
                        'foreignField': '_id',
                        'as': 'equipment'
                    }
            }
        ]
        return [Tariff(**item) async for item in self.collection.aggregate(pipeline=pipeline)]

    async def create(self, item: Tariff) -> Tariff:
        data = item.dict(by_alias=True)
        result = await self.collection.insert_one(item.dict(by_alias=True))
        return await self.get(id=item.id)

    async def get(self, id: UUID) -> Tariff:
        pipeline = [
            {
                '$match': {'_id': id}
            },
            {
                '$lookup':
                    {
                        'from': 'equipment',
                        'localField': 'applicable_to',
                        'foreignField': '_id',
                        'as': 'equipment'
                    }
            },
            {
                '$limit': 1
            }
        ]
        result = [Tariff(**item) async for item in self.collection.aggregate(pipeline=pipeline)]
        return result[0] if len(result) else None

    async def update(self, id: UUID, item: TariffUpdateRequest) -> Tariff:
        result = await self.collection.update_one(
            {'_id': id},
            {
                '$set': item.dict(exclude_unset=True),
                '$currentDate': {'updated_at': True}
            }
        )
        return await self.get(id=id)

    async def delete(self, id: UUID) -> None:
        await self.collection.delete_one({'_id': id})
        return None

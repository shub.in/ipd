from uuid import UUID
from typing import List

from app.schemas.users import User, UserRequest, UserUpdateRequest, PasswordUpdateRequest
from app.store.users import UsersStore

from .base import MongoBaseStore


class UserMongoStore(UsersStore, MongoBaseStore):
    _collection = 'users'

    async def list(self) -> List[User]:
        return [User(**item) async for item in self.collection.find()]

    async def create(self, item: User) -> User:
        data = item.dict(by_alias=True)
        result = await self.collection.insert_one(item.dict(by_alias=True))
        return await self.get(id=item.id)

    async def get(self, id: UUID) -> User:
        document = await self.collection.find_one({'_id': id})
        return User(**document)

    async def get_by_username(self, username: str) -> User:
        document = await self.collection.find_one({'username': username})
        return User(**document) if document is not None else None

    async def update(self, id: UUID, item: UserUpdateRequest) -> User:
        result = await self.collection.update_one(
            {'_id': id},
            {
                '$set': item.dict(exclude_unset=True),
                '$currentDate': {'updated_at': True}
            }
        )
        return await self.get(id=id)

    async def update_password(self, id: UUID, new_password: bytes) -> None:
        result = await self.collection.update_one(
            {'_id': id},
            {
                '$set': {'password': new_password},
                '$currentDate': {'updated_at': True}
            }
        )
        return None

    async def delete(self, id: UUID) -> None:
        await self.collection.delete_one({'_id': id})
        return None

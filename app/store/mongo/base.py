from abc import ABC

from motor.core import AgnosticDatabase, AgnosticCollection

from app.store.base import BaseStore


class MongoBaseStore(BaseStore, ABC):
    _db: AgnosticDatabase
    _collection: str

    def __init__(self, db: AgnosticDatabase):
        self._db = db

    @property
    def collection(self) -> AgnosticCollection:
        return self._db[self._collection]

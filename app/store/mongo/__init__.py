from motor.core import AgnosticDatabase

from app.store import Store
from .abonents import AbonentMongoStore
from .agreements import AgreementMongoStore
from .equipment import EquipmentMongoStore
from .tariffs import TariffMongoStore
from .users import UserMongoStore


class MongoStore(Store):
    abonents: AbonentMongoStore
    agreements: AgreementMongoStore
    equipment: EquipmentMongoStore
    tariffs: TariffMongoStore
    users: UserMongoStore

    def __init__(self, db: AgnosticDatabase):
        self.abonents = AbonentMongoStore(db=db)
        self.agreements = AgreementMongoStore(db=db)
        self.equipment = EquipmentMongoStore(db=db)
        self.tariffs = TariffMongoStore(db=db)
        self.users = UserMongoStore(db=db)

from uuid import UUID
from typing import List

from ...schemas.agreements import Agreement, AgreementUpdateRquest
from ...store.agreements import AgreementStore

from .base import MongoBaseStore


class AgreementMongoStore(AgreementStore, MongoBaseStore):
    _collection = 'agreements'

    async def list(self) -> List[Agreement]:
        return [Agreement(**item) async for item in self.collection.find()]

    async def create(self, item: Agreement) -> Agreement:
        result = await self.collection.insert_one(item.dict(by_alias=True))
        return await self.get(id=item.id)

    async def get(self, id: UUID) -> Agreement:
        document = await self.collection.find_one({'_id': id})
        return Agreement(**document) if document is not None else None

    async def update(self, id: UUID, item: AgreementUpdateRquest) -> Agreement:
        result = await self.collection.update_one(
            {'_id': id},
            {
                '$set': item.dict(exclude_unset=True),
                '$currentDate': {'updated_at': True}
            }
        )
        return await self.get(id=id)

    async def delete(self, id: UUID) -> None:
        await self.collection.delete_one({'_id': id})
        return None

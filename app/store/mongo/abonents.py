from uuid import UUID
from typing import List

from app.schemas.abonents import Abonent, AbonentUpdateRequest
from app.schemas.utils import Pagination
from app.store.abonents import AbonentStore

from .base import MongoBaseStore


class AbonentMongoStore(AbonentStore, MongoBaseStore):
    _collection = 'abonents'

    async def list(self, pagination: Pagination) -> List[Abonent]:
        result = self.collection.find().skip(pagination.offset).limit(pagination.limit)
        return [Abonent(**item) async for item in result]

    async def create(self, item: Abonent) -> Abonent:
        await self.collection.insert_one(item.dict(by_alias=True))
        return await self.get(id=item.id)

    async def get(self, id: UUID) -> Abonent:
        document = await self.collection.find_one({'_id': id})
        return Abonent(**document) if document is not None else None

    async def update(self, id: UUID, item: AbonentUpdateRequest) -> Abonent:
        result = await self.collection.update_one(
            {'_id': id},
            {
                '$set': item.dict(exclude_unset=True),
                '$currentDate': {'updated_at': True}
            }
        )
        return await self.get(id=id)

    async def delete(self, id: UUID) -> None:
        await self.collection.delete_one({'_id': id})
        return None

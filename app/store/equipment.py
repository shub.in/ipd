from abc import abstractmethod, ABC
from uuid import UUID
from typing import List

from app.schemas.equipment import Equipment, EquipmentUpdateRequest

from .base import BaseStore


class EquipmentStore(BaseStore, ABC):
    @abstractmethod
    async def list(self) -> List[Equipment]:
        pass

    @abstractmethod
    async def create(self, item: Equipment) -> Equipment:
        pass

    @abstractmethod
    async def get(self, id: UUID) -> Equipment:
        pass

    @abstractmethod
    async def update(self, id: UUID, item: EquipmentUpdateRequest) -> Equipment:
        pass

    @abstractmethod
    async def delete(self, id: UUID) -> None:
        pass

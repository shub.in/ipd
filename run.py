import uvicorn
from app.main import make_app
from app.config import config, read_config


if __name__ == '__main__':
    config = read_config()
    uvicorn.run(app=make_app(config), host=config.host, port=config.port)

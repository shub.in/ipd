import random

import pytest

from app.dependencies.pagination import get_offset


@pytest.mark.parametrize(
    ['page', 'limit', 'expected'],
    (
            (1, 20, 0),
            (2, 20, 20),
            (3, 20, 40),
            (1, 5, 0),
            (2, 5, 5),
            (3, 5, 10),
    )
)
def test_pager(page, limit, expected):
    items = [x+1 for x in range(100)]
    offset = get_offset(page=page, limit=limit)
    assert offset == expected
    assert len([x for x in items[offset:offset + limit] if offset < x <= offset + limit]) == limit

from starlette import status

from app.schemas.agreements import Agreement, AgreementRequest, AgreementUpdateRquest

from .conftest import TestClient


class TestAgreements:
    def test_create(self, client: TestClient, create_tariff, create_equipment, create_abonent, db_store, event_loop):
        abonent = create_abonent
        equipment = create_equipment
        tariff = create_tariff
        request = AgreementRequest(abonent_id=abonent.id,
                                   tarif_id=tariff.id,
                                   equipmment=[equipment.id])
        r = client.post('/agreements', data=request.json(by_alias=True))
        assert r.status_code == status.HTTP_201_CREATED
        agreement = Agreement(**r.json())
        assert agreement.tarif_id == request.tarif_id
        event_loop.run_until_complete(db_store.agreements.delete(id=agreement.id))

    def test_list(self, client: TestClient, create_agreement):
        agreement = create_agreement
        r = client.get('/agreements')
        assert r.status_code == status.HTTP_200_OK
        found = False
        for item in r.json():
            if Agreement(**item).id == agreement.id:
                found = True
        assert found

    def test_get(self, client: TestClient, create_agreement):
        agreement = create_agreement
        r = client.get(f'/agreements/{agreement.id}')
        assert r.status_code == status.HTTP_200_OK
        assert Agreement(**r.json()).id == agreement.id

    def test_update(self, client: TestClient, create_agreement):
        agreement = create_agreement
        request = AgreementUpdateRquest(tarif_id=agreement.tarif_id)
        r = client.patch(f'/agreements/{agreement.id}', data=request.json(by_alias=True, exclude_unset=True))
        assert r.status_code == status.HTTP_200_OK
        agreement = Agreement(**r.json())
        assert agreement.tarif_id == request.tarif_id

    def test_delete(self, client: TestClient, create_agreement):
        agreement = create_agreement
        r = client.delete(f'/agreements/{agreement.id}')
        assert r.status_code == status.HTTP_204_NO_CONTENT
        not_found = True
        r = client.get('/agreements')
        for item in r.json():
            if Agreement(**item).id == agreement.id:
                not_found = False
        assert not_found

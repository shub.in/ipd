from starlette import status
from faker import Faker

from app.schemas.abonents import Abonent, AbonentRequest, AbonentUpdateRequest

from .conftest import TestClient


class TestAbonents:
    def test_create(self, client: TestClient, faker: Faker, db_store, event_loop):
        request = AbonentRequest(fname=faker.first_name_male(),
                                 lname=faker.last_name_male(),
                                 mname=faker.middle_name_male(),
                                 phone=faker.phone_number(),
                                 passport_issuer=faker.bs(),
                                 passport_number=faker.ssn()[:6],
                                 passport_series=faker.ssn()[:4])
        r = client.post('/abonents', data=request.json(by_alias=True))
        assert r.status_code == status.HTTP_201_CREATED
        abonent = Abonent(**r.json())
        assert abonent.fname == request.fname
        event_loop.run_until_complete(db_store.abonents.delete(id=abonent.id))

    def test_list(self, client: TestClient, create_abonent):
        abonent = create_abonent
        print(abonent)
        r = client.get('/abonents')
        assert r.status_code == status.HTTP_200_OK
        found = False
        for item in r.json():
            if Abonent(**item).id == abonent.id:
                found = True
        assert found

    def test_get(self, client: TestClient, create_abonent):
        abonent = create_abonent
        r = client.get(f'/abonents/{abonent.id}')
        assert r.status_code == status.HTTP_200_OK
        assert Abonent(**r.json()).id == abonent.id

    def test_update(self, client: TestClient, faker: Faker, create_abonent):
        abonent = create_abonent
        request = AbonentUpdateRequest(passport_issuer=faker.bs())
        r = client.patch(f'/abonents/{abonent.id}', data=request.json(by_alias=True, exclude_unset=True))
        assert r.status_code == status.HTTP_200_OK
        abonent = Abonent(**r.json())
        assert abonent.passport_issuer == request.passport_issuer

    def test_delete(self, client: TestClient, create_abonent):
        abonent = create_abonent
        r = client.delete(f'/abonents/{abonent.id}')
        assert r.status_code == status.HTTP_204_NO_CONTENT
        not_found = True
        r = client.get('/abonents')
        for item in r.json():
            if Abonent(**item).id == abonent.id:
                not_found = False
        assert not_found

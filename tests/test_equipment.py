from starlette import status
from faker import Faker

from app.schemas.equipment import Equipment, EquipmentRequest, EquipmentUpdateRequest

from .conftest import TestClient


class TestEquipment:
    def test_create(self, client: TestClient, faker: Faker, db_store, event_loop):
        request = EquipmentRequest(title=faker.file_name(),
                                   price=4000,
                                   initial_fee=1000)
        r = client.post('/equipment', data=request.json(by_alias=True))
        assert r.status_code == status.HTTP_201_CREATED
        equipment = Equipment(**r.json())
        assert equipment.title == request.title
        event_loop.run_until_complete(db_store.equipment.delete(id=equipment.id))

    def test_list(self, client: TestClient, create_equipment):
        equipment = create_equipment
        r = client.get('/equipment')
        assert r.status_code == status.HTTP_200_OK
        found = False
        for item in r.json():
            if Equipment(**item).id == equipment.id:
                found = True
        assert found

    def test_get(self, client: TestClient, create_equipment):
        equipment = create_equipment
        r = client.get(f'/equipment/{equipment.id}')
        assert r.status_code == status.HTTP_200_OK
        assert Equipment(**r.json()).id == equipment.id

    def test_update(self, client: TestClient, create_equipment):
        equipment = create_equipment
        request = EquipmentUpdateRequest(price=3500)
        r = client.patch(f'/equipment/{equipment.id}', data=request.json(by_alias=True, exclude_unset=True))
        assert r.status_code == status.HTTP_200_OK
        equipment = Equipment(**r.json())
        assert equipment.price == request.price

    def test_delete(self, client: TestClient, create_equipment):
        equipment = create_equipment
        r = client.delete(f'/equipment/{equipment.id}')
        assert r.status_code == status.HTTP_204_NO_CONTENT
        not_found = True
        r = client.get('/equipment')
        for item in r.json():
            if Equipment(**item).id == equipment.id:
                not_found = False
        assert not_found

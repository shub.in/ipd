import pytest
from starlette import status
from faker import Faker

from app.schemas.tariffs import Tariff, TariffRequest, TariffUpdateRequest

from .conftest import TestClient


class TestTariffs:
    def test_create(self, client: TestClient, faker: Faker, db_store, event_loop):
        request = TariffRequest(title=faker.cryptocurrency_name(),
                                period=3)
        r = client.post('/tariffs', data=request.json(by_alias=True))
        assert r.status_code == status.HTTP_201_CREATED
        tariff = Tariff(**r.json())
        assert tariff.title == request.title
        event_loop.run_until_complete(db_store.tariffs.delete(id=tariff.id))

    def test_list(self, client: TestClient, create_tariff):
        tariff = create_tariff
        r = client.get('/tariffs')
        assert r.status_code == status.HTTP_200_OK
        found = False
        for item in r.json():
            if Tariff(**item).id == tariff.id:
                found = True
        assert found

    def test_get(self, client: TestClient, create_tariff):
        tariff = create_tariff
        r = client.get(f'/tariffs/{tariff.id}')
        assert r.status_code == status.HTTP_200_OK
        assert Tariff(**r.json()).id == tariff.id

    def test_update(self, client: TestClient, create_tariff):
        tariff = create_tariff
        request = TariffUpdateRequest(title='2 months')
        r = client.patch(f'/tariffs/{tariff.id}', data=request.json(by_alias=True, exclude_unset=True))
        assert r.status_code == status.HTTP_200_OK
        tariff = Tariff(**r.json())
        assert tariff.title == request.title

    def test_delete(self, client: TestClient, create_tariff):
        tariff = create_tariff
        r = client.delete(f'/tariffs/{tariff.id}')
        assert r.status_code == status.HTTP_204_NO_CONTENT
        not_found = True
        r = client.get('/tariffs')
        for item in r.json():
            if Tariff(**item).id == tariff.id:
                not_found = False
        assert not_found


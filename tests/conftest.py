import asyncio
from datetime import datetime, timedelta
from typing import List

import pytest
from fastapi.testclient import TestClient
from faker import Faker

from app.schemas.abonents import Abonent
from app.schemas.agreements import Agreement, Payment
from app.schemas.equipment import Equipment
from app.schemas.tariffs import Tariff
from app.database import MongoDB
from app.config import read_config
from app.main import make_app
from app.store.mongo import MongoStore


@pytest.fixture(scope='session', autouse=True)
def faker_session_locale():
    return ['ru']

@pytest.fixture(scope='session', autouse=True)
def faker_seed():
    return 12345

@pytest.fixture(scope='session')
def event_loop():
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()

@pytest.fixture(scope='session')
def configuration():
    config = read_config()
    config.mongodb_name = f'test_{config.mongodb_name}'
    return config

@pytest.yield_fixture(scope='session')
def client(event_loop, configuration):
    app = make_app(config=configuration)
    with TestClient(app=app) as client:
        yield client

@pytest.fixture(scope='session')
def db_store(event_loop, configuration):
    db = MongoDB(dsn=configuration.db, dbname=configuration.mongodb_name)
    event_loop.run_until_complete(db.connect())
    store = MongoStore(db=db.connection)
    yield store
    event_loop.run_until_complete(db.disconnect())

@pytest.fixture(scope='function')
def create_abonent(faker: Faker, db_store, event_loop):
    request = Abonent(fname=faker.first_name_male(),
                      lname=faker.last_name_male(),
                      mname=faker.middle_name_male(),
                      phone=faker.phone_number(),
                      passport_issuer=faker.bs(),
                      passport_number=faker.ssn()[:6],
                      passport_series=faker.ssn()[:4])
    yield event_loop.run_until_complete(db_store.abonents.create(item=request))
    event_loop.run_until_complete(db_store.abonents.delete(id=request.id))

@pytest.fixture(scope='function')
def create_equipment(faker: Faker, db_store, event_loop):
    request = Equipment(title=faker.file_name(),
                        price=4000,
                        initial_fee=1000)
    yield event_loop.run_until_complete(db_store.equipment.create(item=request))
    event_loop.run_until_complete(db_store.equipment.delete(id=request.id))

@pytest.fixture(scope='function')
def create_tariff(faker: Faker, db_store, event_loop, create_equipment):
    equipment = create_equipment
    request = Tariff(title=faker.cryptocurrency_name(),
                     period=3,
                     applicable_to=[equipment.id])
    yield event_loop.run_until_complete(db_store.tariffs.create(item=request))
    event_loop.run_until_complete(db_store.tariffs.delete(id=request.id))

@pytest.fixture(scope='function')
def create_agreement(faker: Faker, db_store, event_loop, create_tariff, create_equipment, create_abonent):
    abonent = create_abonent
    equipment = create_equipment
    tariff = create_tariff
    payments: List[Payment] = []
    ts = datetime.utcnow()
    for index in range(tariff.period):
        payments.append(Payment(datetime=ts + timedelta(weeks=4 * (index + 1))))
    request = Agreement(abonent_id=abonent.id,
                        equipmment=[equipment.id],
                        tarif_id=tariff.id,
                        payments=payments,
                        number=faker.random_int(min=10000, max=99999))
    yield event_loop.run_until_complete(db_store.agreements.create(item=request))
    event_loop.run_until_complete(db_store.agreements.delete(id=request.id))

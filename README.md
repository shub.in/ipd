# IPD

### Config example

```yaml
title: IPD
db: mongodb://root:example@localhost:27017/admin
mongodb_name: test
host: 0.0.0.0
port: 8000
version: 0.1.0
#api_prefix: /api/ipd
```

### Docker-compose

```bash
docker-compose up -d
```

### Backend

```bash
pipenv install
python run.py
```

### Frontend

```bash
npm -g install yarn
cd ui/
yarn install
npm run dev
```

### DB

UI: http://localhost:8081


### Tests

```bash
pytest tests/.
pytest -vv tests/. # Verbose
```
